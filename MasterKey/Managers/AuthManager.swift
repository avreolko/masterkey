//
//  AuthManager.swift
//  MasterKey
//
//  Created by Valentin Cherepyanko on 07/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class AuthManager: NSObject {
    static let get = AuthManager()
    var authToken : String? = ""
}
