//
//  ViewController.swift
//  MasterKey
//
//  Created by Valentin Cherepyanko on 07/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit
import JSONModel
import LocalAuthentication

class AuthViewController: CoolViewController {
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var authButton: ShapedButton!
    
    var authContext = LAContext()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        passField.hide()
//        authButton.hide()
        // Do any additional setup after loading the view, typically from a nib.
    }

    class AuthRequest: JSONModel {
        var pass: String?
//        init(pass : String) {
//            self.pass = pass
//        }
    }
    
    @IBAction func authorizeButtonTapped(_ sender: Any) {
        self.authorize(self.passField.text!)
    }
    
    func authorize(_ password : String) {
        let request = AuthRequest()
        request.pass = password
        
        LoadingBlocker.show(on: self.view, light: true)
        self.post(ServerSettings.authURL(), json: request.toJSONString()) { (response : NiceResponse?) in
            LoadingBlocker.hide(from: self.view)
            if (response?.statusCode == 200) {
                AuthManager.get.authToken = response?.dataString
                self.show(MyKeys)
            }
        }
    }
    
    var authError : NSError?
    func authByTouch() {
        if authContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            authContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                                       localizedReason: "Touch me, please :)",
                                       reply: {(success: Bool, error: Error?) -> Void in
                                        
                                        if success {
                                            self.authorize("qazxswedcvfrt")
                                        } else {
                                            self.passField.showAnimated()
                                            self.authButton.showAnimated()
                                        }
            })
        } else {
            self.passField.showAnimated()
            self.authButton.showAnimated()
        }
    }
    
    @IBAction func touchButtonTapped(_ sender: Any) {
        authByTouch()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

