//
//  MyKeysController.swift
//  MasterKey
//
//  Created by Valentin Cherepyanko on 07/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class MyKeysController: CoolViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var keys : [Key] = []
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self, action:#selector(self.refresh(sender:)), for:.valueChanged)
        tableView.addSubview(refreshControl)
        
        loadKeys()
    }
    
    func refresh(sender:AnyObject) {
        refreshControl.beginRefreshing()
        // Code to refresh table view
        loadKeys()
    }
    
    func loadKeys() {
        if (!refreshControl.isRefreshing) { LoadingBlocker.show(on:self.view, light:false) }
        
        self.get(ServerSettings.keysURL()) { (response : NiceResponse?) in
            LoadingBlocker.hide(from: self.view)
            self.refreshControl.endRefreshing()
            do {
                try self.keys = Key.arrayOfModels(from: response!.dataString) as! [Key]
            } catch {
                print(error)
            }
            
            self.tableView.reloadData()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Getting the right element
        let key = keys[indexPath.row]
        
        var cell : KeyCell? = tableView.dequeueReusableCell(withIdentifier: "KeyCell") as! KeyCell?
        if (cell == nil) {
            cell = KeyCell(style: .subtitle, reuseIdentifier: "KeyCell")
        }
        
        cell?.configure(key: key)
        
        // Returning the cell
        return cell!
    }
}
