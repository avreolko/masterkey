//
//  AutoresizingButton.m
//  Conf
//
//  Created by Valentin Cherepyanko on 31/07/15.
//  Copyright (c) 2015 Valentin Cherepyanko. All rights reserved.
//

#import "AutoresizingButton.h"

@implementation AutoresizingButton

- (void)awakeFromNib {
    [super awakeFromNib];
    [self resize];
}

- (void)resize {
    CGFloat size = self.frame.size.width > self.frame.size.height ? self.frame.size.width : self.frame.size.height;
    UIImage *resizedImageNormal = [ImageProcessor getResizedImage:[self imageForState:UIControlStateNormal] forSize:size*2];
    [self setImage:resizedImageNormal forState:UIControlStateNormal];
    
    UIImage *resizedImageSelected = [ImageProcessor getResizedImage:[self backgroundImageForState:UIControlStateSelected] forSize:size*2];
    [self setBackgroundImage:resizedImageSelected forState:UIControlStateSelected];
}

- (void)changeImage:(UIImage *)image {
    CGFloat size = self.frame.size.width > self.frame.size.height ? self.frame.size.width : self.frame.size.height;
    UIImage *resizedImageNormal = [ImageProcessor getResizedImage:image forSize:size*2];
    [self setImage:resizedImageNormal forState:UIControlStateNormal];
    
    UIImage *resizedImageSelected = [ImageProcessor getResizedImage:image forSize:size*2];
    [self setBackgroundImage:resizedImageSelected forState:UIControlStateSelected];
}

//- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state {
//    
//    
////    [super setBackgroundImage:image forState:state];
////    [self resize];
//}

@end
