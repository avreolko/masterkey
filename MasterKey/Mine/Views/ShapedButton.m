//
//  ShapedButton.m
//  DEM
//
//  Created by Valentin Cherepyanko on 19/12/14.
//
//

#import "ShapedButton.h"

@implementation ShapedButton

@synthesize cornerRadius = _cornerRadius;

- (void)awakeFromNib {
    [super awakeFromNib];
   
    [self setup];
    
    self.backgroundColor = _color;
    [self addTarget:self action:@selector(didTouchButton) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setup {
    self.layer.cornerRadius = _cornerRadius;
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowRadius = 1;
    self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    
    self.clipsToBounds = YES;
    self.layer.masksToBounds = NO;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = _cornerRadius;
}

- (CGFloat)cornerRadius {
    return _cornerRadius;
}

- (void)didTouchButton {
//    self.backgroundColor = _tapColor;
//    
//    [UIView animateWithDuration:0.5 animations:^{
//        self.backgroundColor = _color;
//    }];
}


//-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    self.contentEdgeInsets = UIEdgeInsetsMake(1.0,1.0,-1.0,-1.0);
//    self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
//    self.layer.shadowOpacity = 0.8;
//    
//    [super touchesBegan:touches withEvent:event];
//    
//}
//
//-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
//    self.contentEdgeInsets = UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
//    self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//    self.layer.shadowOpacity = 0.5;
//    
//    [super touchesEnded:touches withEvent:event];
//    
//}

@end
