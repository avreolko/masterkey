//
//  CoolView.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 17/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolView.h"

@implementation CoolView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setup];
}

- (void)setup {
    [self layoutIfNeeded];
    self.userInteractionEnabled = NO;
    self.layer.cornerRadius = _cornerRadius;
    self.backgroundColor = _color;
    self.backgroundColor = [self.backgroundColor colorWithAlphaComponent:_alpha];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
