//
//  LoadingBlocker.h
//  AutoHelper
//
//  Created by Valentin Cherepyanko on 08/07/16.
//  Copyright © 2016 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimationHelper.h"
#import "UIView+Animations.h"
#import "AdjustableBlurEffect.h"

@interface LoadingBlocker : UIView

@property (weak, nonatomic) IBOutlet UIImageView *activityIndicator;

+ (void)showOn:(UIView *)superView light:(BOOL)light;
+ (void)hideFrom:(UIView *)superView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorHeight;

@end
