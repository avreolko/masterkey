//
//  AutocompleteEntry.m
//  Transport
//
//  Created by Valentin Cherepyanko on 06/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "AutocompleteEntry.h"

@implementation AutocompleteEntry

+ (AutocompleteEntry *)initWithPlaceDict:(NSDictionary *)dict {
    AutocompleteEntry *entry = [AutocompleteEntry new];
    entry.name = dict[@"name"];
    
    NSString *coordString = dict[@"Point"][@"pos"];
    NSArray *stringCoords = [coordString componentsSeparatedByString:@" "];
    NSString *longitudeString = stringCoords.firstObject;
    NSString *latitudeString = stringCoords.lastObject;
    double longitude = longitudeString.doubleValue;
    double latitude = latitudeString.doubleValue;
    entry.location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    return entry;
}

@end
