//
//  TintButton.m
//  SwiftsMobile
//
//  Created by Valentin Cherepyanko on 03/02/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "TintButton.h"

@implementation TintButton

@synthesize customTintColor = _customTintColor;

//- (TintButton *)initWithFrame:(CGRect)frame {
//    self = [super initWithFrame:frame];
//    if (self) {
//        
//    }
//    return self;
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self addTarget:self action:@selector(didTouchButton) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didTouchButton {
    if (_animated) {
        UIView *circle = [UIView new];
        circle.frame = self.bounds;
        circle.backgroundColor = [UIColor lightGrayColor];
        circle.layer.cornerRadius = circle.frame.size.width / 2;
        circle.transform = CGAffineTransformMakeScale(0.3, 0.3);
        [self insertSubview:circle atIndex:0];
        [UIView animateWithDuration:0.75 animations:^{
            circle.alpha = 0;
            circle.transform = CGAffineTransformMakeScale(1, 1);
        } completion:^(BOOL finished) {
            [circle removeFromSuperview];
        }];
    }
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 0.4;
    [self.layer addAnimation:transition forKey:nil];
    [self setNeedsDisplay];
}

- (void)setCustomTint {
    UIImage *newImage = [self.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIGraphicsBeginImageContextWithOptions(self.currentImage.size, NO, newImage.scale);
    [self.customTintColor set];
    [newImage drawInRect:CGRectMake(0, 0, self.currentImage.size.width, newImage.size.height)];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self setImage:newImage forState:UIControlStateNormal];
}

- (void)setCustomTintColor:(UIColor *)customTintColor {
    _customTintColor = customTintColor;
    [self setCustomTint];
}

- (UIColor *)customTintColor {
    return _customTintColor;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
