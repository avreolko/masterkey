//
//  LoadingBlocker.m
//  AutoHelper
//
//  Created by Valentin Cherepyanko on 08/07/16.
//  Copyright © 2016 Valentin Cherepyanko. All rights reserved.
//

#import "LoadingBlocker.h"

@implementation LoadingBlocker

+ (void)showOn:(UIView *)superView light:(BOOL)light {
    [LoadingBlocker hideFrom:superView];
    
    LoadingBlocker *view = [[[NSBundle mainBundle] loadNibNamed:@"LoadingBlocker" owner:self options:nil] objectAtIndex:0];
    
    view.frame = CGRectMake(0, 0, superView.frame.size.width, superView.frame.size.height);
    
    view.layer.cornerRadius = superView.layer.cornerRadius;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionReveal; //choose your animation
    [view.layer addAnimation:transition forKey:nil];
    
    if (!light) {
        view.backgroundColor = [UIColor whiteColor];
    } else {
        view.indicatorWidth.constant = 20;
        view.indicatorHeight.constant = 20;
        view.backgroundColor = [UIColor whiteColor];
    }
    
    [view hide];
    [superView addSubview:view];
    [view showAnimated];
    [AnimationHelper rotateThis:view.activityIndicator durationTime:2.0];
}

+ (void)hideFrom:(UIView *)superView {
    for(UIView *aView in superView.subviews){
        if([aView isKindOfClass:[LoadingBlocker class]]){
            [UIView animateWithDuration:0.3 animations:^{
                aView.alpha = 0;
            } completion:^(BOOL finished) {
                [[NSNotificationCenter defaultCenter] removeObserver:self];
                [aView removeFromSuperview];
            }];
        }
    }
}

@end
