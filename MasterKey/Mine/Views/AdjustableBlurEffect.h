//
//  AdjustableBlurEffect.h
//  AutoHelper
//
//  Created by Valentin Cherepyanko on 08/07/16.
//  Copyright © 2016 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdjustableBlurEffect : UIBlurEffect

+ (instancetype)effectWithStyle:(UIBlurEffectStyle)style;

@end
