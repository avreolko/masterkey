//
//  TintButton.h
//  SwiftsMobile
//
//  Created by Valentin Cherepyanko on 03/02/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TintButton : UIButton

@property IBInspectable UIColor *customTintColor;
@property IBInspectable BOOL animated;

@end
