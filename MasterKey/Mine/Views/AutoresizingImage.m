//
//  AutoresizingImage.m
//  Conf
//
//  Created by Valentin Cherepyanko on 31/07/15.
//  Copyright (c) 2015 Valentin Cherepyanko. All rights reserved.
//

#import "AutoresizingImage.h"

@implementation AutoresizingImage

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat size = self.frame.size.width > self.frame.size.height ? self.frame.size.width : self.frame.size.height;
    self.image = [ImageProcessor getResizedImage:self.image forSize:size*2];
}

- (void)changeImage:(UIImage *)image {
    CGFloat size = self.frame.size.width > self.frame.size.height ? self.frame.size.width : self.frame.size.height;
    self.image = [ImageProcessor getResizedImage:image forSize:size*4];
}

- (void)resizeByImage:(UIImage *)image {
    self.frame = CGRectMake(0, 0, image.size.width/2, image.size.height/2);
}

- (void)loadImage:(NSString *)urlString {
    if (!self.activityIndicator) {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 12, 12)];
        self.activityIndicator.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self addSubview:self.activityIndicator];
        self.activityIndicator.hidesWhenStopped = YES;
    }
    [self.activityIndicator startAnimating];
    
    UIImage *oldImage = self.image;
    NSURL *url = [NSURL URLWithString:urlString];
    [self sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image) [self changeImage:oldImage];
        [self.activityIndicator stopAnimating];
    }];
}

@end
