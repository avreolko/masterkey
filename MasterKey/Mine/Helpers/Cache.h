//
//  Cache.h
//  Transport
//
//  Created by Valentin Cherepyanko on 19/11/15.
//  Copyright © 2015 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSUserDefaults+RMSaveCustomObject.h"

#define CACHE_KEY @"AT_TIMING_CACHE"

@interface Cache : NSObject

@property NSMutableDictionary *objects;

@property NSString *transportRefreshToken;
@property NSString *stopsRefreshToken;

+ (Cache *)get;
- (void)save;
- (void)load;
- (void)setObject:(id)object forKey:(NSString *)key;

@end
