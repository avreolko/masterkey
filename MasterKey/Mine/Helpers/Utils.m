//
//  Utils.m
//  DEM
//
//  Created by Valentin Cherepyanko on 16/12/14.
//
//

#import "Utils.h"

@implementation Utils

+ (CGSize)screenSize {
    return [UIScreen mainScreen].bounds.size;
}

+ (void)addShadowToView:(UIView *)view {
//    view.layer.shadowColor = [UIColor grayColor].CGColor;
//    view.layer.shadowOffset = CGSizeMake(0, 0);
//    view.layer.shadowOpacity = 1;
//    view.layer.shadowRadius = 3.0;
//    view.clipsToBounds = NO;
//    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    view.layer.shadowRadius = 0;
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowOpacity = 0.1;
    view.layer.shouldRasterize = YES;
}

+ (UIColor *)getColorFromString:(NSString *)string {
    if ([string isEqualToString:@""] || string == nil) {
//        NSLog(@"Color is unset!");
        return nil;
    } else {
        unsigned rgbValue = 0;
        NSScanner *scanner = [NSScanner scannerWithString:string];
        [scanner setScanLocation:1]; // bypass '#' character
        [scanner scanHexInt:&rgbValue];
        return [self getColorFromInt:rgbValue];
    }
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    if (![hexString isEqualToString:@""] && hexString != nil && ![hexString isEqualToString:@"#"]) {
        unsigned int rgbValue = 0;
        NSScanner *scanner = [NSScanner scannerWithString:hexString];
        [scanner setScanLocation:1]; // bypass '#' character
        [scanner scanHexInt:&rgbValue];
        return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    } else {
        return nil;
    }
}

+ (UIColor *)getColorFromInt:(int)number {
    UIColor *color = [UIColor colorWithRed:((float)((number & 0xFF0000) >> 16))/255.0 \
                                     green:((float)((number & 0x00FF00) >>  8))/255.0 \
                                      blue:((float)((number & 0x0000FF) >>  0))/255.0 \
                                     alpha:1.0];
    
    return color;
}

+ (void)setNewFrameFor:(UIView *)view withSize:(CGSize)size {
    float coef = size.width/size.height;
    view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.width/coef);
}

//+ (float)getHeightForLabel:(UILabel *)label {
//    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
//    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constrain lineBreakMode:NSLineBreakByWordWrapping];
//    return size.height;
//}

+ (NSString *)removeHTMLfromString:(NSString *)string {
    NSRange r;
    while ((r = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        string = [string stringByReplacingCharactersInRange:r withString:@""];
    return string;
}

+ (UIViewController*)topController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

+ (UIView *)topView {
    return [[UIApplication sharedApplication] keyWindow];
}

+ (UILabel *)createLabelWithText:(NSString *)text
                               x:(CGFloat)x
                               y:(CGFloat)y
                       textColor:(int)color
                        fontSize:(CGFloat)fontSize
                           width:(CGFloat)width {
    
    UIFont *customFont = [UIFont fontWithName:@"OpenSans" size:fontSize]; //custom font
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, 30)];
    label.numberOfLines = 0;
    label.textColor = [Utils getColorFromInt:color];
    label.font = customFont;
    label.text = text;
    [label sizeToFit];
    return label;
}

+ (UILabel *)rotateLabel:(UILabel *)label rotation:(CGFloat)transformNumber {
    [label setTransform:CGAffineTransformMakeRotation(transformNumber)];
    return label;
}

+ (NSString *)getFormattedDate:(NSDate *)date {
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setLocale:locale];
    [df setDateStyle:NSDateFormatterMediumStyle];
    
    [df setDateFormat:@"dd MMMM"];
    
//    [df setTimeStyle:NSDateFormatterShortStyle];
    return [df stringFromDate:date];
}

+ (NSDate *)getDateFromString:(NSString *)string withFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter dateFromString:string];
}

+ (NSString *)getFormattedDateS:(NSString *)dateString withFormat:(NSString *)format {
    NSDate *date = [self getDateFromString:dateString withFormat:format];
    return [self getFormattedDate:date];
}

+ (NSString *)getFormattedTimeString:(NSDate *)time {
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm"];
    return [timeFormatter stringFromDate:time];
}

+ (UIColor *)randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

+ (NSArray *)getUniqueArray:(NSArray *)array {
    return [[array valueForKeyPath:@"@distinctUnionOfObjects.self"] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
}

+ (UIImage *)imageFromLabel:(UILabel *)label {
    CGSize size = [label bounds].size;
    size.height += 1;
    size.width += 1;
    
    UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    
    [[label layer] renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (void)printFonts {
    for (NSString *familyName in [UIFont familyNames]){
        NSLog(@"Family name: %@", familyName);
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            NSLog(@"--Font name: %@", fontName);
        }
    }
}

+ (UIImage *)takeScreenshot {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = [keyWindow bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

+ (BOOL)validateEmail:(NSString *)email {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

+ (double)roundedDoubleFromString:(NSString *)string {
//    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
//    [fmt setPositiveFormat:@"0.##"];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    [f setMaximumFractionDigits:2];
    [f setRoundingMode: NSNumberFormatterRoundUp];
    
    NSNumber *number = [f numberFromString:string];
    return number.doubleValue;
}

+ (NSString *)formatNumberString:(NSString*)originalString {
    originalString = [originalString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setGroupingSeparator:@" "];
    NSNumber *myNumber = [formatter numberFromString:originalString];
    return [formatter stringFromNumber:myNumber];
}

+ (NSNumber *)numberFromString:(NSString *)string {
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    return [formatter numberFromString:string];
}

+ (int)round:(int)number to:(int)rounder {
    return rounder * ((number + rounder / 2) / rounder);
}

+ (NSString *)getChangedDateWithDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)generateUUID {
    CFUUIDRef udid = CFUUIDCreate(NULL);
    NSString *udidString = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
    return udidString;
}

+ (NSString *)hoursAndMinutesFromMinutes:(long)minutes {
    long hours = minutes / 60;
    long minutesDiv = minutes % 60;
    
    NSString *fieldText = [NSString new];
    if (hours > 0) {
        fieldText = [NSString stringWithFormat:@"%ldч", hours];
    }
    
    if (minutesDiv > 0) {
        fieldText = [NSString stringWithFormat:@"%@ %ldм", fieldText, minutesDiv];
    }
    
    return fieldText;
}

+ (float)getHeightForLabel:(UILabel *)label {
    CGSize constrain = CGSizeMake(label.bounds.size.width, FLT_MAX);
    
    return [label sizeThatFits:constrain].height;
}

+ (NSString *)stringFromDates:(NSDate *)date1 second:(NSDate *)date2 {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    return [NSString stringWithFormat:@"%@—%@", [dateFormatter stringFromDate:date1], [dateFormatter stringFromDate:date2]];
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter stringFromDate:date];
}

@end
