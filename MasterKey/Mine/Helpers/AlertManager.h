//
//  AlertManager.h
//  AutoHelper
//
//  Created by Valentin Cherepyanko on 16/07/16.
//  Copyright © 2016 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define LOGIN_INVALID @"login_invalid"
#define FILL_TIME @"fillTime"

@interface AlertManager : NSObject

+ (AlertManager *)get;
- (UIViewController *)alertForStatus:(NSString *)status;
- (UIAlertController *)alertWithOptions:(NSArray *)actions;

@property NSMutableDictionary *messages;

@end
