//
//  NotificationHelper.m
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 31/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "NotificationHelper.h"
#import "Utils.h"

@implementation NotificationHelper

//+ (NotificationHelper *)get {
//    static NotificationHelper *instance = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        instance = [[self alloc] init];
//    });
//    return instance;
//}

+ (void)addLocalNotificationAtTime:(NSString *)timeString message:(NSString *)message {
    NSDate *time = [Utils getDateFromString:timeString withFormat:@"HH:mm"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:time];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    NSDate *oldDate = [NSDate date]; // Or however you get it.
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:oldDate];
    comps.hour   = hour;
    comps.minute = minute;
    NSDate *newDate = [calendar dateFromComponents:comps];
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = newDate;
    localNotification.alertBody = message;
    localNotification.timeZone = [NSTimeZone localTimeZone];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

@end
