//
//  AnimationHelper.m
//  RutokenTest
//
//  Created by Valentin Cherepyanko on 12/03/15.
//  Copyright (c) 2015 Aktiv Co. All rights reserved.
//

#import "AnimationHelper.h"

@implementation AnimationHelper

+ (void)rotateThis:(UIView *)view durationTime:(float)duration {
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = @0.0f;
    animation.toValue = @(-2*M_PI);
    animation.duration = duration;
    animation.repeatCount = HUGE_VALF;
    [view.layer addAnimation:animation forKey:@"rotation"];
}
//@(-2*M_PI)

+ (void)rotateThis:(UIView *)view durationTime:(float)duration byAngle:(id)angle {
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = @0.0f;
    animation.toValue = angle;
    animation.duration = duration;
    animation.repeatCount = 1;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [view.layer addAnimation:animation forKey:@"rotation"];
}


// DICH
+ (void)rotateUP:(UIView *)view duration:(float)duration {
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = @0.0f;
    animation.toValue = @(-M_PI);
    animation.duration = duration;
    animation.repeatCount = 1;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [view.layer addAnimation:animation forKey:@"rotation"];
}

+ (void)rotateDown:(UIView *)view duration:(float)duration {
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = @(-M_PI);
    animation.toValue = @(-2*M_PI);
    animation.duration = duration;
    animation.repeatCount = 1;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [view.layer addAnimation:animation forKey:@"rotation"];
}
// END OF DICH

+ (void)stopAnimatingThis:(UIView *)view {
    [view.layer removeAllAnimations];
}

+ (void)hideThis:(UIView *)view delay:(float)delay durationTime:(float)duration callback:(void (^)(void))callback {
    view.alpha = 1;
    [UIView animateWithDuration:duration delay:delay options:(UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         view.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         if (callback) callback();
                     }];
}

+ (void)showThis:(UIView *)view delay:(float)delay durationTime:(float)duration callback:(void (^)(void))callback {
    [UIView animateWithDuration:duration delay:0 options:(UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         view.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         if (callback) callback();
                     }];
}

+ (POPSpringAnimation *)move:(UIView *)view on:(CGPoint)point {
//    [view pop_removeAllAnimations];
//    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
//    positionAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(view.center.x + point.x, view.center.y + point.y)];
//    positionAnimation.springBounciness = 8;
//    positionAnimation.springSpeed = 5;
//    return positionAnimation;
    
    [view pop_removeAllAnimations];
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    positionAnimation.toValue = [NSValue valueWithCGRect:CGRectOffset(view.frame, point.x, point.y)];
    positionAnimation.springBounciness = 8;
    positionAnimation.springSpeed = 5;
    return positionAnimation;
}

+ (POPSpringAnimation *)popupAnimation {
    POPSpringAnimation *sizeAnim = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    sizeAnim.fromValue = [NSValue valueWithCGSize:CGSizeMake(0, 0)];
    sizeAnim.toValue = [NSValue valueWithCGSize:CGSizeMake(1, 1)];
    sizeAnim.springBounciness = 10;
    sizeAnim.springSpeed = 20;
    return sizeAnim;
}

+ (POPSpringAnimation *)popdownAnimation {
    POPSpringAnimation *sizeAnim = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    sizeAnim.fromValue = [NSValue valueWithCGSize:CGSizeMake(1, 1)];
    sizeAnim.toValue = [NSValue valueWithCGSize:CGSizeMake(0, 0)];
    sizeAnim.springBounciness = 0;
    sizeAnim.springSpeed = 50;
    return sizeAnim;
}

+ (POPSpringAnimation *)bounceAnimation {
    POPSpringAnimation *bounce = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    bounce.fromValue = [NSValue valueWithCGSize:CGSizeMake(0.9, 0.9)];
    bounce.toValue = [NSValue valueWithCGSize:CGSizeMake(1, 1)];
    bounce.springBounciness = 20;
    return bounce;
}

+ (void)xBounce:(UIView *)view {
    [view.layer pop_removeAnimationForKey:@"positionAnimation"];
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
    positionAnimation.velocity = @400;
    positionAnimation.springBounciness = 20;
    [view.layer pop_addAnimation:positionAnimation forKey:@"positionAnimation"];
}

@end
