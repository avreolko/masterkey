//
//  NotificationHelper.h
//  AT-Timing
//
//  Created by Valentin Cherepyanko on 31/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationHelper : NSObject

//+ (NotificationHelper *)get;
+ (void)addLocalNotificationAtTime:(NSString *)timeString message:(NSString *)message;
@end
