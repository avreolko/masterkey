//
//  CoolViewController.m
//  Calca
//
//  Created by Valentin Cherepyanko on 02/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import "CoolViewController.h"

@implementation CoolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.blockView = self.view;
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return self;
}

- (void)keyboardWillShow:(NSNotification *)notification {
    if (_keyboardTricks) [self.view move:CGPointMake(0, -105)];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    if (_keyboardTricks) [self.view move:CGPointMake(0, 105)];
}

- (void)prepare {
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
}

- (void)passData:(id)object {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)push:(NSString *)cname {
    [self push:cname withData:nil];
}

- (void)push:(NSString *)cname withData:(id)object {
//    CoolViewController *presentingC = [[NSBundle mainBundle] loadNibNamed:cname owner:self options:nil].firstObject;
    CoolViewController *presentingC = [[NSClassFromString(cname) alloc] initWithNibName:cname bundle:nil];
    [presentingC prepare];
    if (object) [presentingC passData:object];
    
    [self.navigationController.view.layer addAnimation:presentingC.pushTransition forKey:kCATransition];
    [self.navigationController pushViewController:presentingC animated:YES];
}

- (void)present:(NSString *)cname {
    [self present:cname withData:nil];
}

- (void)present:(NSString *)cname withData:(id)object {
    CoolViewController *presentingC =  [[NSClassFromString(cname) alloc] initWithNibName:cname bundle:nil];
    [presentingC prepare];
    if (object) [presentingC passData:object];
    
    [self.navigationController.view.layer addAnimation:presentingC.pushTransition forKey:kCATransition];
    [self.navigationController presentViewController:presentingC animated:YES completion:nil];
}

- (void)POST:(NSURL *)url json:(NSString *)json completion:(void (^)(NiceResponse *response))callback {
//    [LoadingBlocker showOn:self.blockView light:YES];
    [[ServerRequestManager new] POSTRequestWithURL:url andData:json completion:^(NiceResponse *response) {
//        [LoadingBlocker hideFrom:self.blockView];
        
        if (response.statusCode == 200) callback(response);
        else {
            callback(response);
            NSLog(@"CHECK THIS! RESPONSE STATUS: %ld", response.statusCode);
        }
    }];
}

- (void)GET:(NSURL *)url completion:(void (^)(NiceResponse *response))callback {
//    [LoadingBlocker showOn:self.blockView light:NO];
    [[ServerRequestManager new] GETRequestWithURL:url completion:^(NiceResponse *response) {
//        [LoadingBlocker hideFrom:self.blockView];
        
        if (response.statusCode == 200) callback(response);
        else NSLog(@"CHECK THIS! RESPONSE STATUS: %ld", response.statusCode);
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController.view.layer addAnimation:self.popTransition forKey:kCATransition];
}

// override this for custom effect
- (CATransition *)pushTransition {
    return nil;
}

- (CATransition *)popTransition {
    return nil;
}

- (BOOL)checkFields {
    BOOL allOK = YES;
    for (UITextField *field in self.fieldsToCheck) {
        if (field.text.length == 0 && field.superview.alpha == 1) {
            [AnimationHelper xBounce:field];
            allOK = NO;
        }
    }
    
    return allOK;
}

- (void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:_hideNavigationBar animated:YES];
}

- (IBAction)close:(id)sender {
    [self pop];
}

- (void)pop {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)show:(NSString *)segue {
    [self performSegueWithIdentifier:segue sender:self];
}

@end
