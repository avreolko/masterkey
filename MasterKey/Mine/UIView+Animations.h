//
//  AnimatedViewController.h
//  Conf
//
//  Created by Valentin Cherepyanko on 31/08/15.
//  Copyright (c) 2015 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimationHelper.h"
#import "AdjustableBlurEffect.h"

@interface UIView (Animations)

- (void)hide;
- (void)show;
- (void)hideAnimated;
- (void)showAnimated;
- (void)hideAnimatedWithCompletion:(void (^)())callback;
- (void)showAnimatedWithCompletion:(void (^)())callback;

- (void)popup;
- (void)popdown;

- (void)move:(CGPoint)point;
- (void)addBlur;
- (void)bounce;

@end
