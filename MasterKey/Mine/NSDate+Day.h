//
//  NSDate+Day.h
//  Calca
//
//  Created by Valentin Cherepyanko on 10/02/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate(Day)

- (NSDate*)day;

@end
