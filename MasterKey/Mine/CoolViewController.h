//
//  CoolViewController.h
//  Calca
//
//  Created by Valentin Cherepyanko on 02/03/17.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ServerRequestManager.h"
#import "UIView+Animations.h"
#import "AnimationHelper.h"
#import "Utils.h"
#import "Defines.h"
#import "ImageProcessor.h"
#import "ServerRequestManager.h"
#import "NiceResponse.h"
#import "ServerSettings.h"
#import "UILabel+FormattedText.h"
#import "AdjustableBlurEffect.h"
#import "AutoresizingButton.h"
#import "AutoresizingImage.h"
#import "LoadingBlocker.h"
#import "ShapedButton.h"
#import "NSDate+Day.h"
#import "TransitionHelper.h"
#import "UILabel+FormattedText.h"
#import "AlertManager.h"
#import "CoolTextView.h"
#import "AutoComlpeteView.h"
#import "Cache.h"

#define MyKeys @"MyKeysController"

@interface CoolViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *fieldsToCheck;
@property UIView *blockView;
@property UIModalTransitionStyle transitionStyle;

@property IBInspectable BOOL hideNavigationBar;
@property IBInspectable BOOL keyboardTricks;
@property IBOutlet UIView *mainView;

- (void)prepare;

- (BOOL)checkFields;

- (void)push:(NSString *)cname;
- (void)push:(NSString *)cname withData:(id)object;
- (void)present:(NSString *)cname;
- (void)present:(NSString *)cname withData:(id)object;

- (void)POST:(NSURL *)url json:(NSString *)json completion:(void (^)(NiceResponse *response))callback;
- (void)GET:(NSURL *)url completion:(void (^)(NiceResponse *response))callback;

- (CATransition *)pushTransition;
- (CATransition *)popTransition;
- (void)passData:(id)object;
- (void)pop;

- (void)show:(NSString *)segue;

@end
