//
//  ServerSettings.h
//  DEM
//
//  Created by Valentin Cherepyanko on 04/12/14.
//
//


#define SERVER_ADDRESS @"cheerry.ru"
//#define SERVER_ADDRESS @"localhost"

#define HTTP_PORT 10030

#define AUTH_URL @"auth"
#define KEYS_URL @"keys"

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ServerSettings : NSObject

+ (NSURL *)authURL;
+ (NSURL *)keysURL;

@end
