//
//  ServerSettings.m
//  DEM
//
//  Created by Valentin Cherepyanko on 04/12/14.
//
//

#import "ServerSettings.h"

@implementation ServerSettings

+ (NSString *)getServerAddressString {
    if (HTTP_PORT > 0 && HTTP_PORT < 65535) {
        return [NSString stringWithFormat:@"http://%@:%d", SERVER_ADDRESS, HTTP_PORT];
    } else {
        return [NSString stringWithFormat:@"http://%@", SERVER_ADDRESS];
    }
}

+ (NSURL *)authURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], AUTH_URL];
    return [NSURL URLWithString:urlString];
}

+ (NSURL *)keysURL {
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self getServerAddressString], KEYS_URL];
    return [NSURL URLWithString:urlString];
}

@end
