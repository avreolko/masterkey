//
//  KeyCell.swift
//  MasterKey
//
//  Created by Valentin Cherepyanko on 07/04/2017.
//  Copyright © 2017 Valentin Cherepyanko. All rights reserved.
//

import UIKit

class KeyCell: UITableViewCell {
    @IBOutlet weak var panel: UIView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var entityLabel: UILabel!
    @IBOutlet weak var keyEntity: UIView!
    @IBOutlet weak var loginPass: UIView!
    
    @IBOutlet weak var keyEntityY: NSLayoutConstraint!
    var key : Key?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loginPass.hide()
        self.keyEntity.layer.cornerRadius = 5
        // Initialization code
    }

    func configure(key : Key) {
        self.key = key
        entityLabel.text = key.name
        loginLabel.text = key.login
        passLabel.text = key.pass
    }
    
    @IBAction func copyButtonTapped(_ sender: Any) {
        UIPasteboard.general.string = key!.pass
    }
    
    @IBAction func eyeButtonTapped(_ sender: Any) {
        showData()
    }
    
    func showData() {
        if (self.loginPass.alpha > 0) {
            self.passLabel.text = self.key!.pass
        }
        
        self.keyEntityY.constant = -self.keyEntity.bounds.height;
        UIView.animate(withDuration: 0.4, animations: {
            self.loginPass.alpha = 1;
            self.keyEntity.alpha = 0;
            self.panel.layoutIfNeeded()
        }) { (completed : Bool) in
            let delay = Int(1 * Double(1000))
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
                self.keyEntityY.constant = 0;
                UIView.animate(withDuration: 0.4, animations: {
                    self.loginPass.alpha = 0;
                    self.keyEntity.alpha = 1;
                    self.panel.layoutIfNeeded()
                }) { (completion: Bool) in
//
                }
            }
        }
    }
    
    @IBAction func cellTapped(_ sender: Any) {
//        panel.bounce()
    }
}
